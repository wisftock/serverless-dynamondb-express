const AWS = require("aws-sdk");
const { translateArray } = require("./helpers/translate");

exports.handler = async (event) => {
  const dynamodb = new AWS.DynamoDB.DocumentClient();

  const db = await dynamodb
    .scan({
      TableName: "softtekTable",
    })
    .promise();

  const startwars = await fetch("https://swapi.py4e.com/api/people");
  const response = await startwars.json();
  const { films, species, vehicles, starships, created, edited, url, ...data } =
    response.results;

  const newData = Object.values(data);
  const info = [...db.Items, ...newData];
  const result = translateArray(info, "es");

  console.log("result", result);
  return {
    message: "Lista de personajes",
    status: 200,
    body: { result },
  };
};
