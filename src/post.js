const AWS = require("aws-sdk");
const crypto = require("node:crypto");

exports.handler = async (event) => {
  const { name, height, mass, hair_color, skin_color, eye_color, birth_year } =
    JSON.parse(event.body);

  const dynamodb = new AWS.DynamoDB.DocumentClient();

  const id = crypto.randomUUID().toString();

  const data = {
    id,
    name,
    height,
    mass,
    hair_color,
    skin_color,
    eye_color,
    birth_year,
  };

  const params = {
    TableName: "softtekTable",
    Item: data,
  };

  await dynamodb.put(params).promise();

  return {
    message: "Creado con éxit wo",
    status: 200,
    body: data,
  };
};
