const translateArray = (arr, lang) => {
  return arr.map((obj) => translate(obj, lang));
};

const translate = (obj, lang) => {
  const translations = {
    name: {
      en: "name",
      es: "nombre",
    },
    height: {
      en: "height",
      es: "altura",
    },
    mass: {
      en: "mass",
      es: "masa",
    },
    hair_color: {
      en: "hair_color",
      es: "color_de_cabello",
    },
    skin_color: {
      en: "skin_color",
      es: "color_de_piel",
    },
    eye_color: {
      en: "eye_color",
      es: "color_de_ojos",
    },
    birth_year: {
      en: "birth_year",
      es: "año_de_nacimiento",
    },
    gender: {
      en: "gender",
      es: "género",
    },
    homeworld: {
      en: "homeworld",
      es: "planeta_natal",
    },
  };

  const translatedObj = {};

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      const translation = translations[key];
      const translatedKey = translation ? translation[lang] || key : key;
      translatedObj[translatedKey] = obj[key];
    }
  }

  return translatedObj;
};

module.exports = {
  translateArray,
};
